import os
import shutil
import re
import fnmatch
import time

import filecmp



class SortFile:
    __all__ = ["removeFile", "cutFile", "copyFile","searchFiles", "searchAllFiles","size"]
    def __init__(self):
        # private variables 
        self._files = {}
        self._canceled = False
        self.compiledFiles =[]
        # public variable
        self.size = 0

    def _save_cut(self,src,dst,size,name):
        self._files[name] = {"src": dst,
                            "dst" :os.path.dirname(src),
                            "size" : size,
                            "name" : name
                            }

    def clearSavedFiles(self):
        self._files = {}
    
    def removeFile(self,path):
        if os.path.isfile(path):
            try:
                os.remove(path)
                return f"{path} removed"
            except PermissionError:
                return f"Permission Error:{path} not removed"
        else:
            return f"{path} not exist"
 

    def cutFile(self,fromPath,destPath):
        """
                nouve à file
                fromPath : string
                destPath : string        
        """
        if  not os.path.exists(destPath):
            return f"{destPath} Invavid dest Path"     
        if not os.path.exists(fromPath):
            return f"{fromPath} Invavid from Path"
       # verify if the file ixist in dest dir
        exist = self._exist_in_dest(fromPath,destPath,True)
        if exist[0]:
            return exist[1]
        else:
            renamed = exist[1]
            
        if os.path.isfile(fromPath):
            s  = os.path.getsize(fromPath)
            try:
                shutil.copy2 (fromPath,destPath)# copy les fichier  dans le dossier donné
                shutil.copystat (fromPath,destPath)# copy les métadonné du fichier
                if not self._canceled:
                    name = os.path.basename(fromPath)
                    self._save_cut(fromPath,destPath,s,name)
                else:
                    if self._files[os.path.basename(fromPath)]["name"] != os.path.basename(fromPath) :
                        try:
                            os.rename(os.path.join(destPath,os.path.basename(fromPath)),
                                                   os.path.join(destPath,self._files[os.path.basename(fromPath)]["name"]))
                        except OSError as e:
                                      print("cannot rename file")
                    self._files.pop(os.path.basename(fromPath))
                    self.size -= s
                try:
                    os.remove(fromPath)
                    return f"Success : {fromPath} --> {destPath} {renamed}" 
                except PermissionError:
                    return f"Alert : {fromPath} --> {destPath}\nAccèss denied :  {fromPath} {renamed}" 
            except OSError as e:
                return "Error : {e} \n{fromPath} --> {destPath} {renamed}" 
        else:
            return f"Error : {fromPath} not a file {renamed}"
            
    
                    
    def _exist_in_dest(self,src,dst,cut=False):
        found = False
        basename = os.path.basename 
        renamed_number = 1
        split_tup = os.path.splitext(basename(src))
        file_name = split_tup[0]
        file_extension = split_tup[1]
        file = os.path.join(dst,basename(src))
        while not found :
            if os.path.exists(file):
                if filecmp.cmp(file,src):
                    return [True, f"Alert : {src} exist in {dst} as name {basename(file)}"]
                else:
                    renamed_number+=1
            else:
                found = True
            file = os.path.join(dst,file_name+f'-{renamed_number}'+file_extension)
        if renamed_number > 1:
            os.rename(os.path.join(dst,basename(src)),file)
            if cut:
                self._files[basename(file)] = self._files[basename(src)]
                self._files.pop(basename(src))
            return [False, f"   |  {os.path.join(dst,basename(src))} has renamed to {file}"]
        else:
            return [False,""]


    def copyFile(self,fromPath,destPath):
        """ Copie the file fromParth to the file or directory destPath """
        if not os.path.exists(destPath):
            return f"{destPath} Invavid dest Path"
        if not os.path.exists(fromPath):
            return f"{fromPath} Invavid from Path"
        renamed = ""
        # verify if the file ixist in dest dir
        exist = self._exist_in_dest(fromPath,destPath)
        if exist[0]:
            return exist[1]
        else:
            renamed = exist[1]
        
        if os.path.isfile(fromPath):
            try:
                shutil.copy2 (fromPath,destPath)# copy les fichier  dans le dossier donné
                shutil.copystat (fromPath,destPath)# copy les métadonné du fichier
                return f"Success : {fromPath} --> {destPath} {renamed}"
            except OSError as e:
                return "Error : {e} \n{fromPath} --> {destPath} {renamed}" 
        else:
            return f"Error : {fromPath} n'est pas un fichier {renamed}"
 
        
    def cancelCutFiles(self):
        """ """
        #copyFiles = self._files.copy()
        print(self._files)
        self._canceled = True
        if self._files == {}:
            yield f"Empty file list"
        else:
            for element in self._files.copy():
                namefile = element
                if element == self._files[element]["name"]:
                    namefile = self._files[element]["name"]
                src = os.path.join(self._files[element]['src'],namefile)
                dst = self._files[element]['dst']
                size = self._files[element]['size']
                if not os.path.exists(src):
                    yield f"{src} has been deleted"
                else:
                    if not os.path.exists(dst):
                        yield f"{dst} has been deleted it will created"
                        os.makedirs(dst)
                    if os.path.exists(os.path.join(dst,namefile)):
                        yield f"{namefile} already exist in {dst}"
                    else:
                      yield self.cutFile(src,dst)
            self._canceled = True
        yield f"cancel move files finsish"
                
    def _searchFiles(self,top,Pattern,recursive=False,isRegEx=False):
        """ top : String
            Pattern : String
            recursive : Boolean
            isRegex: Boolean
            return : False or list of filesname """
        if not isinstance(top,str):
            return False
        if not isinstance(Pattern,str):
            return False
        if not isinstance(recursive,bool):
            return False
        if not isinstance(isRegEx,bool):
            return False
        
        regEx = None
        size_file = os.path.getsize
        joinPath = os.path.join
        #realPath = os.path.abspath
        if isRegEx :
            try:
                regEx = re.compile(Pattern).match
            except re.error:
                pass
        else:
            Pattern = fnmatch.translate(Pattern.lower())
            regEx = re.compile(Pattern).match
        if regEx != None:
            for root, dirs, files in os.walk(top, topdown=True):
                for name in files:
                    if not isRegEx :
                        name = os.path.normcase(name)
                    if root != top :
                        if recursive and regEx(name):
                            s = size_file(joinPath(root,name))
                            self.size += s
                            yield {"size" : s,"file" : joinPath(root, name) }
                    else: 
                        if regEx(name) :
                            s = size_file(joinPath(root,name))
                            self.size += s
                            yield {"size" : s,"file" : joinPath(root, name) }        
        else:
            yield False
            
    def searchFiles(self,top,Pattern,recursive=False,isRegEx=False):
        """
            top : String
            Pattern : String
            recursive : Boolean
            isRegex: Boolean
            return : False or list of filesnames
        """
        self.size = 0
        yield from self._searchFiles(top,Pattern,recursive,isRegEx)
        #return list(self._searchFiles(top,Pattern,recursive,isRegEx))


    def searchAllFiles(self,top,Patterns,recursive=False,isRegEx=False):
        """
            top : String
            Pattern : list of Strings
            recursive : Boolean
            isRegex: Boolean
            return : False or list of filesnames
        """
        ## if you need use size variable in progress bar for exemple,
        ## before make your operation stock your search in variable
        ## Exemple:
        ## T = Trie()
        ## search = list(T.searchAllFiles(src,Pattern,False,False))
        ## T.size will be exact 
        ## for file in search:
        ##      T.copyFile(file['file'],dst)
        ##
        ## if you donn't need use size, you can do this
        ## Exemple:
        ## T = Trie()
        ## for file in T.searchAllFiles(src,Pattern,False,False):
        ##      T.copyFile(file['file'],dst)
        res = {}
        if not isinstance(Patterns,list):
            return res
        inter_size = 0
        for pat in Patterns:
            yield from self.searchFiles(top,pat,recursive,isRegEx)
            #res[pat] =  self.searchFiles(top,pat,recursive,isRegEx)
            inter_size += self.size
        self.size = inter_size
        
        #return res

    def compile(self,src,Patterns,recursive=False,isRegEx=False):
        """ """
        self._compiledFiles = list(self.searchAllFiles(src,Patterns,recursive,isRegEx))
        if len(self._compiledFiles) > 0:    
            if self._compiledFiles[0] == False:
                self._compiledFiles = []
                return f"Compilation Error"
            else:
                return f"{len(self._compiledFiles)} files match"
        else:
            return f"0 files match"
        
    def run(self,dst,cut=False):
        if not isinstance(dst,str):
            return f"dst not corectly formated"
        if not os.path.isdir(dst):
            return f"{dst} is not a directory"
        for file in self._compiledFiles:
                self.size -= file['size']
                if cut:
                    yield  self.cutFile(file['file'],dst)
                else:
                    yield  self.copyFile(file['file'],dst)
                
        

    def sort(self,src,dst,Patterns,cut=False,recursive=False,isRegEx=False):
        """ """
        if not isinstance(cut,bool):
            return f"cut must be a boolean"
        if not isinstance(dst,str):
            return f"dst not corectly formated"
        if not os.path.isdir(dst):
            return f"{dst} is not a directory"
        for file in self.searchAllFiles(src,Patterns,recursive,isRegEx):
            if cut:
                yield self.cutFile(file['file'],dst)
            else:
                yield self.copyFile(file['file'],dst)
        


def test(): 
    T = SortFile()
    src = 'C:/Users/DENISSE-BEN/Desktop/TestDirectory/'
    os.mkdir('OUT')
    dst = os.path.join(src,'OUT')
    tim = time.time()
    Patterns = ["*.avi"]
    print("#"*10," Compile ","#"*10)
    print(T.compile(src,Patterns,True,False))
    print("size",T.size)
    print("#"*10," Run ","#"*10)
    for x in T.run(dst,cut=True):
        print(x)
    print("#"*10," Sort ","#"*10)
    for x in T.sort(src,dst,Patterns,True,True,False):
        print(x)


def test2():
    T = SortFile()
    src = "C:/Users/DENISSE-BEN/Desktop/mes document/Nouveau dossier (4)/TRIE/t/TRIE/OUT"
    print(T.compile(src,['*.png'],False,False))
    
    

        
    
    
    

#test()
