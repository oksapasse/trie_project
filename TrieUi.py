import os
import glob
import shutil
import trieClass as TS

from math import ceil

DEPENDENCIES = ["ttkthemes"]
        
    
################################### GESTION DE LA FENETRE ##################################################################
import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import*
#from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askdirectory



class Version(object):
    """
    Parses a semantic version string.
    """
    def __init__(self, string):
        """
        :param string: semantic version string (major.minor.patch)
        """
        elements = tuple(map(int, string.split(".")))
        if len(elements) == 3:
            self.major, self.minor, self.patch = elements
        else:
            (self.major, self.minor), self.patch = elements, 0
        self.version = (self.major, self.minor, self.patch)

    def __ge__(self, other):
        return all(elem1 >= elem2 for elem1, elem2 in zip(self.version, other.version))




try :
    from ttkthemes import ThemedTk, THEMES
except ImportError as e:
    from pip import __version__ as pip_version
    if Version(pip_version) >= Version("10.0.0"):
        import pip._internal as pip
    else:
        import pip

    # Install requirements with pip
    pip.main(["install"] + DEPENDENCIES + ["-U"])
    from ttkthemes import ThemedTk, THEMES
    

#from ttkwidgets import ScaleEntry
#from ttkwidgets.autocomplete import AutocompleteCombobox











def new():
    f = (Aplication())


#######################################################  DOCUMENTATION ####################################################################################################


class Window(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title('aide')
        ttk.Label(self,text= """
                            Ce petit programme nomée Trie
                            permet de trier vos fichiers
                            selon un motif recherché
                            renseigner le repertoire source
                            et le repertoire de destination
                            et enfin le motif recherchée
                            puis validez
                                

                                """).pack(side="top")
        ttk.Label(self,text="""copyrigth@Denisse Ben
                                  12/2021
                                  """).pack(side="bottom")

#######################################################  STYLE ####################################################################################################
        
class Preference(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title('preferences')
        self.selected_theme = tk.StringVar()
        self.parent = parent
        theme_frame = ttk.LabelFrame(self, text='Themes')
        theme_frame.grid(padx=10, pady=20, ipadx=20, ipady=20, sticky='w',row=2, column=2)     
        
        for theme_name in THEMES:
            rb = ttk.Radiobutton(
                theme_frame,
                text=theme_name,
                value=theme_name,
                variable=self.selected_theme,
                command=self.change)
            rb.pack(expand=True, fill='both')

    def change(self):
        theme = self.selected_theme.get()
        self.parent.set_theme(theme)
        self.parent.update()    



        

class Aplication(ThemedTk):      
    def __init__(self,theme='blue'):
        self._Func = TS.SortFile()
        self.create_Windows(theme)
        self.grid_windows()
        
          

############################### PROCEDURE D'ANNULATION EN CAS DE DEPLACEMENT DE FICHIERS IMPORTANTES ######################################
        
    def cancel(self):
        """annulation"""
        self.writeOnResultTextWidget("canceling ...")
        for msg in self._Func.cancelCutFiles() : 
            self.writeOnResultTextWidget(msg)
                
##################################3# FONCTION PRINCIPALE #########################################

        
        

    

########################### FONCTION DES OPTIONS MENU #####################################################

    def callback(self):
        "procedure qui recharge la fenetre"
        if askyesno('alert', 'Êtes-vous sûr de vouloir faire une nouvelle recherche ?'):
            self.destroy()
            return(new())
                
        else:
            pass
                
    def quitter (self):
        "procedure qui ferme la fenetre du programme "
        if askyesno('alert','Etes vous sûr de vouloir quitter ?'):
            self.destroy()
        else:
            pass

################## PROCEDURE VALIDATION ET VERIFICATION DES PARAMETRE FOURNIT #######################   
                
    def valider(self):
        """ procedure qui appel au fonction trie en lui fournissant les pearamètre necessaire pour faire les recherches et trier les fichier"""
        src = self.src.get()
        dst = self.dst.get()
        Patterns = [self.patern.get()]
        cut = self.cut.get()
        sub_dir = self.sub_dir.get()
        isRegEx = self.isRegEx.get()
        self.recherche(src,dst,Patterns,sub_dir,cut,isRegEx)
            
            

###################### GESTION DE LA SELECTION DES REPERTOIRES #############################################        
                
    def choixrep(self,nom):
        "procedure qui insere les chemeins des repertoires dan les champs de saisie de la fenetre "
        rep = askdirectory(title='Choisissez un répertoire')
        if nom == 'src':
            self.src.delete(0,tk.END)
            self.src.insert(tk.END,rep)
        elif nom == 'dst':
            self.dst.delete(0,tk.END)
            self.dst.insert(tk.END,rep)
            
    def src1 (self):
        "procedure qui renvoie a l'insertion du dossier source dans le champ de saisie"
        self.choixrep('src')

    def dst1(self):
        "procedure qui renvoie à l'insertion du dossier destination dans le champ de saisie"
        self.choixrep('dst')

    def fast_search(self,Patterns):
        src = self.src.get()
        dst = self.dst.get()
        cut = self.cut.get()
        sub_dir = self.sub_dir.get()
        self.recherche(src,dst,Patterns,sub_dir,cut,isRegEx=False)
        
        
    def documents(self):
        """ procedure fait la recherche de tout les documents du dossier passé en parametre et les trie dans le repretoire de destination"""

        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les fichiers documents ?'):
            self.fast_search(['*.txt','*.xml','*.doc','*.docx','*.odf','*.odt','*.docx', '*.xslx', '*.pptx','*.pdf','*.xls', '*.ptt','*.rtf'])

            
            
    def audio (self):
        "procedure qui recherche les fichiers audio d'un dossier source et ou dans ses sous dossiers pour les trier dans  le repertoire de destination"
        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les fichiers audio ?'):
            self.fast_search(['*.mp3','*.wav','*.wma','*.ogg','*.oga','*.ogv','*.ogx', '*.flac', '*.aac','*.m4a','*.au', '*.aif','*.wav'])
            
            
    def video(self):
        "procedure qui recherche les fichiers videos d'un dossier source et ou dans ses sous dossiers pour les trier dans  le repertoire de destination"
        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les fichier video ?'):
            self.fast_search(['*.mp4','*.avi','*.3gp','*.wmv','*.mov','*.xvid','*.mkv', '*.mka', '*.mks','*.mpeg4','*.flv', '*.mpeg 4'])
            
            
    def photo(self):
        "procedure qui recherche les fichiers images/photo d'un dossier source et ou dans ses sous dossiers pour les trier dans  le repertoire de destination"
        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les fichier image/Photo ?'):
            self.fast_search(['*.jpg','*.png','*.jpeg','*.tiff','*.bmp','*.gif','*.raw', '*.ico','*.bmp','*.tif','*.ppm'])
           
            
    def exe (self):
        "procedure qui recherche les fichiers executables d'un dossier source et ou dans ses sous dossiers pour les trier dans  le repertoire de destination"
        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les fichier executables ?'):
            self.fast_search(['*.exe'])
            
            
    def programation(self):
        "procedure qui recherche les fichiers de programmtion d'un dossier source et ou dans ses sous dossiers pour les trier dans  le repertoire de destination"
        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les fichiers de programmme  ?'):
            self.fast_search(['*.c','*.py','*.js','*.css','*.html','*.php', '*.sql', '*.sh','*.bsh','*.h', '*.hpp',
                              '*.hxx','*.cpp','*.cxx','*.cc','*.cs','*.htm','*.hta','*.xhtml','*.shtm','*.shtlm',
                              '*.java','*.pyw','*.ini','*.inif','*.vbs','*.vb'])
            
            
    def compresse(self):
        "procedure qui recherche les fichiers archives/compressée d'un dossier source et ou dans ses sous dossiers pour les trier dans  le repertoire de destination"
        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les fichiers compressée ?'):
            self.fast_search(['*.7z','*.ACE','*.ARC','*.ARJ','*.bzip2','*.CAB','*.gzip','*.LHA','*.LZH','*.RAR','*.UHA' ,
                              '*.XZ' ,'*.Z','*.Zip','*.cab','*.lha','*.arj','*.arc',
                             '*.lzh','*.rar','*.uha','*.xz','*.z'])
            
        
    def disque(self):
        "procedure qui recherche les fichiers disque d'image d'un dossier source et ou dans ses sous dossiers pour les trier dans  le repertoire de destination"
        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les fichiers images disques ?'):
            self.fast_search(['*.iso','*.mdf','*.mds','*.img','*.dmg','*.mdx','*.vcd','*.cdd','*.nrg','*.vfd','*.pid' ,
                              '*.tib' ,'*.bin','*.cue'])
            
            
    def tout(self):
        "procedure qui recherche les types de fichiers d'un dossier source et ou dans ses sous dossiers pour les trier dans  le repertoire de destination"
        if askyesno('Attention','Êtes vous sûr de vouloir rechercher tout les types de fichiers ?'):
            self.fast_search(['*.txt','*.xml','*.doc','*.docx','*.odf','*.odt','*.docx', '*.xslx', '*.pptx','*.pdf',
                              '*.xls', '*.ptt','*.rtf','*.mp3','*.wav','*.wma','*.ogg','*.oga','*.ogv','*.ogx', '*.flac',
                              '*.aac','*.m4a','*.au', '*.aif','*.wav','*.mp4','*.avi','*.3gp','*.wmv','*.mov','*.xvid',
                              '*.mkv', '*.mka', '*.mks','*.mpeg4','*.flv', '*.mpeg 4','*.jpg','*.png','*.jpeg','*.tiff',
                              '*.bmp','*.gif','*.raw', '*.ico','*.bmp','*.tif','*.ppm','*.exe','*.c','*.py','*.js','*.css',
                              '*.html','*.php', '*.sql', '*.sh','*.bsh','*.h', '*.hpp','*.hxx','*.cpp','*.cxx','*.cc',
                              '*.cs','*.htm','*.hta','*.xhtml','*.shtm','*.shtlm','*.java','*.pyw','*.ini','*.inif','*.vbs',
                              '*.vb','*.7z','*.ACE','*.ARC','*.ARJ','*.bzip2','*.CAB','*.gzip','*.LHA','*.LZH','*.RAR',
                              '*.UHA' ,'*.XZ' ,'*.Z','*.Zip','*.cab','*.lha','*.arj','*.arc','*.lzh','*.rar','*.uha',
                              '*.xz','*.z','*.iso','*.mdf','*.mds','*.img','*.dmg','*.mdx','*.vcd','*.cdd','*.nrg',
                              '*.vfd','*.pid' ,'*.tib' ,'*.bin','*.cue'])
            
            
            
            
    def documentation(self):
        window = Window(self)
        window.grab_set()

    def preference(self):
        p = Preference(self)
        p.grab_set()


    def writeOnResultTextWidget(self,chaine):
        #print("isCalled")
        self.resultat['state'] = "normal"
        self.last_line_result+=1 
        self.resultat.insert(str(self.last_line_result)+".0"," $ " + chaine + "\n")
        self.resultat['state'] = "disabled"
        self.resultat.yview(self.last_line_result)
        print(str(self.last_line_result) + " $ " + chaine + "\n\n")
        
    def update_progress_label(self):
        return f"{ceil(self.pb['value'])}%"


    
    def progress(self):
        if(self.totalSize > 0):
            if self.sizeProgress > 0:
                val = self.sizeProgress * 100 / self.totalSize
                if self.pb['value'] < 100:
                    self.pb['value'] = val
                    self.pb_label['text'] = self.update_progress_label()
                else:
                    self.pb['value'] = 0.0
                    #showinfo(message='The progress completed!')
            else:
                self.pb['value'] = 0.0
        else:
            pass

        

    def maj_total_size_of_copy(self,val):
        self.totalSize = val
    def maj_size_progress_of_copy(self,val):
        self.sizeProgress = val


    def recherche(self,src,dst,Patterns,sub_dir,cut,isRegEx):
        
        file = self._Func.compile(src,Patterns,sub_dir,isRegEx)
        self.writeOnResultTextWidget(file)
        self.maj_total_size_of_copy(self._Func.size)
        self.update()
        for write in self._Func.run(dst,cut):
            self.writeOnResultTextWidget(write)
            self.maj_size_progress_of_copy(self.totalSize - self._Func.size)
            self.progress()
            self.update()
        self.writeOnResultTextWidget("finsished")
        
        
                
            

        
    ############################## GESTION DE LA FENETRE ########################################
    def create_Windows(self,theme):
        
        ThemedTk.__init__(self, fonts=True, themebg=True)
        self.set_theme(theme)
        self.title('Trie')

        self.menubar = tk.Menu(self)
        # Menu 
        self.menu1 = tk.Menu(self.menubar, tearoff=0)
        self.menu1.add_command(label="nouvelle Recherche ", command=self.callback)
        self.menu1.add_command(label="annuler couper ",command=self.cancel)
        self.menu1.add_separator()
        self.menu1.add_command(label="Quitter", command=self.quitter)
        self.menubar.add_cascade(label="Fichier", menu=self.menu1)

        self.menu2 = tk.Menu(self.menubar, tearoff=0)
        self.menu2.add_command(label="A propos de l'utilisation", command=self.documentation)
        self.menubar.add_cascade(label="Aide", menu=self.menu2)

        self.menuPreference = tk.Menu(self.menubar, tearoff=0)
        self.menuPreference.add_command(label="Style", command=self.preference)
        self.menubar.add_cascade(label="Preference", menu=self.menuPreference)

        ######################## CONTENUE DE LA FENETRE #################################
        self.config(menu=self.menubar)

        # LABELS
        self.sub_title = ttk.Label(self,text="Rechercher et tirer vos fichiers ",font="BOLD") 
        self.global_msg = ttk.Label(self,text='Designation',font="BOLD")
        self.msg_src = ttk.Label(self,text='Repertoire source ')
        self.msg_dst = ttk.Label(self,text='Repertoire destination')
        self.msg_patern = ttk.Label(self,text='Motif')
        

        # VARIABLES  OF INPUT

        self.cut = tk.BooleanVar()
        self.sub_dir = tk.BooleanVar()
        self.isRegEx = tk.BooleanVar()
        self.dir = tk.StringVar()

        self.last_line_result = 0 # line where message will be write
        
        # INPUT 
        
        self.src = tk.Entry(self,width=80)
        self.dst = tk.Entry(self,width=80)
        self.patern = tk.Entry(self,width=80)
        
        self.btn_cut = ttk.Checkbutton(self, text="couper",
                                       variable=self.cut,onvalue=True,
                                       offvalue=False)
        
        self.btn_sub_dir = ttk.Checkbutton(self, text="sous dossiers",
                                           variable=self.sub_dir,onvalue=True,
                                           offvalue=False)
        
        self.btn_regEx = ttk.Checkbutton(self, text="regex",
                                         variable=self.isRegEx,
                                         onvalue=True,offvalue=False)

        self.get_src_btn = ttk.Button(self,text="source",command=self.src1)
        self.get_dst_btn = ttk.Button(self,text="destination",command=self.dst1)
        
        self.validate_btn = ttk.Button(self,text='valider',width=20,command=self.valider)

        # OUTPUT
        self.resultat = tk.Text(self,fg='white',bg='black')
        self.scrollbar = ttk.Scrollbar(self, orient='vertical', command=self.resultat.yview)

        #  communicate back to the scrollbar
        self.resultat['yscrollcommand'] = self.scrollbar.set
        self.resultat['state'] = "disabled"

        # Partie fenetre de recherche de fichiers de type documents audio musique photo.......
        self.msg_fast_search = ttk.Label(self,text='Recherches rapides',font="ITALIC")
        self.search_doc = ttk.Button(self,text='Documents',command=self.documents)
        self.search_audio = ttk.Button(self,text='Audios',command=self.audio)
        self.search_video = ttk.Button(self,text='Videos',command=self.video)
        self.search_image = ttk.Button(self,text='Images',command=self.photo)
        self.search_programme = ttk.Button(self,text='Programmes',command=self.programation)
        self.search_exe = ttk.Button(self,text='Executables',command=self.exe)
        self.search_zip = ttk.Button(self,text='Compressés',command=self.compresse)
        self.search_disk_img = ttk.Button(self,text='Images disques',command=self.disque)
        self.search_all = ttk.Button(self,text='Fichiers',command=self.tout)


        self.pb = ttk.Progressbar(self,orient=tk.HORIZONTAL,maximum=100,length=400,mode='determinate')
        self.pb_label = ttk.Label(self,text=' 0 %')

        self.writeOnResultTextWidget("\t" *4 + "#"* 4 + " Bienvenue "+"#" *4 + "\t" *4)
        

    def grid_windows(self):
        sticky = {"sticky": "nswe"}
        # MSG LABEL
        self.sub_title.grid(row=0,column=0)
        self.global_msg.grid(row=0,column=2,**sticky)
        self.msg_src.grid(row=1,column=2,**sticky)
        self.msg_dst.grid(row=2,column=2,**sticky)
        self.msg_patern.grid(row=3,column=2,**sticky)

        # BUTTON
        self.btn_cut.grid(row =4 ,ipadx=10,sticky= tk.W)
        self.btn_sub_dir.grid(row =4 ,ipadx=10,sticky= tk.E)
        self.btn_regEx.grid(row =3,column=1 ,ipadx=10,sticky= tk.W)
        self.get_src_btn.grid(row=1,column=1,padx=10)
        self.get_dst_btn.grid(row=2,column=1,padx=10)
        self.validate_btn.grid(row = 5,column=0,pady=10)

        # INPUT 
        self.src.grid(row=1,pady=10)
        self.dst.grid(row=2,pady=10)
        self.patern.grid(row =3,pady=10 )

        self.resultat.grid(row=6, column=0, sticky='ew' ,rowspan=4)
        self.scrollbar.grid(row=6, column=1, sticky='ns', rowspan=4)

        # FAST SEARCH
        self.msg_fast_search.grid(row=0,column=3,padx=10)
        self.search_doc.grid(row=1,column=3,**sticky,padx=10, pady =10)
        self.search_audio.grid(row=2,column=3,**sticky,padx =10,pady =10)
        self.search_video.grid(row=3,column=3,**sticky,padx=10,pady =10)
        self.search_image.grid(row=4,column=3,**sticky,padx=10,pady =10)
        self.search_programme.grid(row=5,column=3,**sticky,padx=10,pady =10)
        self.search_exe.grid(row=6,column=3,**sticky,padx=10,pady =10)
        self.search_zip.grid(row=7,column=3,**sticky,padx=10,pady =10)
        self.search_disk_img.grid(row=8,column=3,**sticky,padx=10,pady =10)
        self.search_all.grid(row=9,column=3,**sticky,padx=10,pady =10)
        

        # PROGRESS BAR
        self.pb.grid(row=10,column=0,columnspan=3, padx=5, pady=5,sticky="ew")
        self.pb_label.grid(row=10,column=3,**sticky)

        


    
        

########################################################################################################################################################


if __name__ == '__main__':
    start = Aplication()
    start.set_theme("adapta")
    start.mainloop()
    



    
